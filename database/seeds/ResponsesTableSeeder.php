<?php

use Illuminate\Database\Seeder;

class ResponsesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('respons')->insert([ //Defines what table will be filled.
          ['response_id' => 1, 'response_text' => 'Response 1', 'question_id' => 1], //Values are inputted into related columns, left = column, right = value ('left' => 'right)'
          ['response_id' => 2, 'response_text' => 'Response 2', 'question_id' => 2],
          ['response_id' => 3, 'response_text' => 'Response 3', 'question_id' => 3],
        ]);
    }
}
