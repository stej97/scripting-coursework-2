<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(QuestionTableSeeder::class); //Calls what seeder will be called.
        $this->call(QuestionnaireTableSeeder::class);
        $this->call(ResponsesTableSeeder::class);
    }
}
