<?php

use Illuminate\Database\Seeder;

class QuestionnaireTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('questionnaires')->insert([ //Defines what table will be filled.
          ['questionnaire_id' => 1, 'questionnaire_title' => 'Questionnaire 1'],//Values are inputted into related columns, left = column, right = value ('left' => 'right)'.
          ['questionnaire_id' => 2, 'questionnaire_title' => 'Questionnaire 2'],
          ['questionnaire_id' => 3, 'questionnaire_title' => 'Questionnaire 3'],
        ]);
    }
}
