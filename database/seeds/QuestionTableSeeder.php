<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('questions')->insert([ //Defines what table will be filled.
          ['question_id' => 1, 'question_text' => 'Question 1'], //Values are inputted into related columns, left = column, right = value ('left' => 'right)'.
          ['question_id' => 2, 'question_text' => 'Question 2'],
          ['question_id' => 3, 'question_text' => 'Question 3'],
        ]);
    }
}
