<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\question;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $questions = question::all(); //Variable questions holds all values within question table

      return view('questionnaire', ['questions' => $questions]);//Returns questionnaire.blade.php and all question table values store into the $questions variable.
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.questions.create');//Returns create view from folders admin then questions. 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $this->validate($request, [
          'question_text' => 'required', //
      ]);

      $input = $request->all();

      question::create($input);

      return redirect('questions');
    }

    /*
   * Secure the set of pages to the admin.
   */
  public function __construct()
  {
      $this->middleware('auth'); //Auth is iomplemented to secure contents related to this controller.
  }
}
