<?php

  namespace App\Http\Controllers;

  use Illuminate\Http\Request;

  use App\Http\Requests;
  use App\Http\Controllers\Controller;
  use App\question;
  use App\respons;

  class QuestionnaireController extends Controller
  {
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function index()
      {

        $questions = question::all();//Reteives all data from question table.

        return view('questionnaire', ['questions' => $questions]);//Returns questionnaire.blade.php and all question table values store into the $questions variable.
      }


      public function create()
       {
           return view('admin.questions.response');//Returns response view from categories, admin then questions.
       }

       public function edit($id)
       {
           $questions = question::findOrFail($id);

           return view('admin.questions.edit', compact('questions'));
       }

      public function store(Request $request)
 {
     $input = $request->all();

     respons::create($input);

     return redirect('/');
 }




}
