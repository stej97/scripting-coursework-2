<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\respons;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $response = respons::all(); //Stores all response table data into the $response variable.

      return view('admin.questions.response', ['response' => $response]); //Returns view response from folder admin then questions. Response table data is stored into $response variable. 
    }
    /*
   * Secure the set of pages to the admin.
   */
  public function __construct()
  {
      $this->middleware('auth'); //Auth is iomplemented to secure contents related to this controller.
  }
}
