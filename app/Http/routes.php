<?php

  // ===============================================
  // STATIC PAGES ==================================
  // ===============================================

  // show a static view for your home page (app/resources/views/home.blade.php)

  Route::resource('/', 'QuestionnaireController');




  // about page (app/resources/views/questionnaire.blade.php)

  Route::group(['middle' => ['web']], function () {

    Route::auth();

    Route::get('/home', 'HomeController@index');
    Route::resource('/response', 'ResponseController');
    Route::resource('/questions', 'QuestionsController');

    });
