<!doctype html>
  <html>
  <head>
      <meta charset="UTF-8">
      <title>Questionnaire</title>
      <link rel="stylesheet" href="/css/app.css" />
  </head>
  <body>
  <div class="container">
      <header class="row">
          <nav class="navbar navbar-inverse navbar-fixed-top">
              <div class="container">
                  <ul class="nav navbar-nav">
                      <li class="active"><a href="/home">Admin Login</a></li>
                  </ul>
              </div>
          </nav>
      </header>
      <article class="row">
      <h1>Questionnaire</h1>

<!-- Question table output -->
<div id="questions" style="float: left; margin-top: 2%;">
      @if (isset ($questions))

          <ol>
              @foreach ($questions as $question)
                  <li>{{ $question->question_text }}</li><br>
              @endforeach
          </ol>
      @else
          <p> no questions added yet </p>
      @endif
</div>
<!-- End of Question table output -->

<!-- Answer input form -->
      {!! Form::open() !!}

<div id="answers" style="float: left;  margin-top: 2%; margin-left: 5%;">
      <div class="form-group">
      {!! Form::label('response_text', 'Question 1:') !!}
      {!! Form::select('response_text', array('Yes' => 'Yes', 'No' => 'No', '3' => '***', '4' => '****', '5' => '*****'), null,['placeholder' => 'Select...']) !!}
      </div>

      <div class="form-group">
      {!! Form::label('response_text', 'Question 2:') !!}
      {!! Form::select('response_text', array('1' => '*', '2' => '**', '3' => '***', '4' => '****', '5' => '*****'), null,['placeholder' => 'Select...']) !!}
      </div>

      <div class="form-group">
      {!! Form::label('response_text', 'Question 3:') !!}
      {!! Form::select('response_text', array('1' => '*', '2' => '**', '3' => '***', '4' => '****', '5' => '*****'), null,['placeholder' => 'Select...']) !!}
      </div>

      <div class="form-group">
      {!! Form::label('response_text', 'Question 4:') !!}
      {!! Form::select('response_text', array('1' => '*', '2' => '**', '3' => '***', '4' => '****', '5' => '*****'), null,['placeholder' => 'Select...']) !!}
      </div>

      <div class="form-group">
      {!! Form::label('response_text', 'Question 5:') !!}
      {!! Form::select('response_text', array('1' => '*', '2' => '**', '3' => '***', '4' => '****', '5' => '*****'), null,['placeholder' => 'Select...']) !!}
      </div>

      <div class="form-group" style="width: 50%;">
      {!! Form::submit('Submit', ['class' => 'btn btn-primary form-control']) !!}
      </div>
</div>

      {!! Form::close() !!}
<!-- End of Answer input form -->

</div>

</body>
</html>
