<!doctype html>
  <html>
  <head>
      <meta charset="UTF-8">
      <title>Admin - create questions</title>
      <link rel="stylesheet" href="/css/app.css" />
  </head>
  <body>
  <div class="container">
      <header class="row">
          <nav class="navbar navbar-inverse navbar-fixed-top">
              <div class="container">
                  <ul class="nav navbar-nav">
                      <a class="navbar-brand" href="#">Admin</a>
                      <li class="active"><a href="/">Questionnaire</a></li>
                      <li class="active"><a href="/response">Response</a></li>
                      <li class="active"><a href="/home">Dashboard</a></li>
                  </ul>
              </div>
          </nav>
      </header>
      
      <question class="row">
          <h1>Create a new question</h1>

      @if ($errors->any())
        <div>
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif

    <!-- Create Question Form -->
    {!! Form::open(['url' => 'questions']) !!}

    <div class="form-group" style="width: 50%">
        {!! Form::label('question_text', 'Question text:') !!}
        {!! Form::text('question_text', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group"  style="width: 10%">
        {!! Form::submit('Add Question', ['class' => 'btn btn-primary form-control']) !!}
    </div>

    {!! Form::close() !!}

    <!-- End of Question Form -->

  </question>

  </div><!-- close container -->

  </body>
  </html>
