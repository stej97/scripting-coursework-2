@extends('layouts.master')
  @section('title', 'Responses')
  @section('content')
  <header class="row">
      <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
              <ul class="nav navbar-nav">
                  <a class="navbar-brand" href="#">Admin</a>
                  <li class="active"><a href="/">Questionnaire</a></li>
                  <li class="active"><a href="/questions/create">Create Question</a></li>
                  <li class="active"><a href="/home">Dashboard</a></li>
              </ul>
          </div>
      </nav>
  </header>
      <h1>Questionnaire Responses</h1>

<!-- Response List -->
  <section>
      @if (isset ($response))

          <ul>
              @foreach ($response as $response)
                  <li>{{ $response->response_text }}</li>
              @endforeach
          </ul>
      @else
          <p> no responses added yet </p>
      @endif
  </section>
  @endsection
<!-- End of Response List -->
