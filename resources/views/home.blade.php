@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Dashboard</div>
                <ul class="nav navbar-nav">
                    <li class="active"><button><a href="/questions/create">Create Question</a></button></li>
                    <li class="active"><button><a href="/response">Reponses</a></button></li>                    
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
